<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
  <sch:ns prefix="f" uri="http://hl7.org/fhir"/>
  <sch:ns prefix="h" uri="http://www.w3.org/1999/xhtml"/>
  <!-- 
    This file contains just the constraints for the profile АжиглалтМн
    It includes the base constraints for the resource as well.
    Because of the way that schematrons and containment work, 
    you may need to use this schematron fragment to build a, 
    single schematron that validates contained resources (if you have any) 
  -->
  <sch:pattern>
    <sch:title>f:Observation</sch:title>
    <sch:rule context="f:Observation">
      <sch:assert test="count(f:partOf) &lt;= 0">partOf: maximum cardinality of 'partOf' is 0</sch:assert>
      <sch:assert test="count(f:category) &gt;= 1">category: minimum cardinality of 'category' is 1</sch:assert>
      <sch:assert test="count(f:category) &lt;= 1">category: maximum cardinality of 'category' is 1</sch:assert>
      <sch:assert test="count(f:focus) &lt;= 0">focus: maximum cardinality of 'focus' is 0</sch:assert>
      <sch:assert test="count(f:issued) &lt;= 0">issued: maximum cardinality of 'issued' is 0</sch:assert>
      <sch:assert test="count(f:interpretation) &gt;= 1">interpretation: minimum cardinality of 'interpretation' is 1</sch:assert>
      <sch:assert test="count(f:interpretation) &lt;= 1">interpretation: maximum cardinality of 'interpretation' is 1</sch:assert>
      <sch:assert test="count(f:bodySite) &lt;= 0">bodySite: maximum cardinality of 'bodySite' is 0</sch:assert>
      <sch:assert test="count(f:method) &lt;= 0">method: maximum cardinality of 'method' is 0</sch:assert>
      <sch:assert test="count(f:hasMember) &lt;= 0">hasMember: maximum cardinality of 'hasMember' is 0</sch:assert>
      <sch:assert test="count(f:derivedFrom) &lt;= 0">derivedFrom: maximum cardinality of 'derivedFrom' is 0</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation</sch:title>
    <sch:rule context="f:Observation">
      <sch:assert test="not(parent::f:contained and f:contained)">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол, тэрээр дотроо багтсан хэрэглүүр агуулах ЁСГҮЙ. (inherited)</sch:assert>
      <sch:assert test="not(exists(for $id in f:contained/*/f:id/@value return $contained[not(parent::*/descendant::f:reference/@value=concat('#', $contained/*/id/@value) or descendant::f:reference[@value='#'])]))">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол, тэрээр хэрэглүүрийн хаа нэгтэйгээс заагдсан байх ЁСТОЙ эсвэл агуулж буй хэрэглүүр рүү заах ЁСТОЙ. (inherited)</sch:assert>
      <sch:assert test="not(exists(f:contained/*/f:meta/f:versionId)) and not(exists(f:contained/*/f:meta/f:lastUpdated))">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол тэрээр meta.versionId эсвэл meta.lastUpdated элементтэй байх ЁСГҮЙ. (inherited)</sch:assert>
      <sch:assert test="not(exists(f:contained/*/f:meta/f:security))">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол, аюулгүй байдлын шошготой байх ЁСГҮЙ. (inherited)</sch:assert>
      <sch:assert test="exists(f:text/h:div)">Зохион байгуулалтыг сайн байлгах үүднээс хэрэглүүрүүдэд хүүрнэл хэсэг байх хэрэгтэй. (inherited)</sch:assert>
      <sch:assert test="not(exists(f:dataAbsentReason)) or (not(exists(*[starts-with(local-name(.), 'value')])))">Ажиглалт.утга[X] байхгүй үед л өгөгдөлӨгөгдөөгүйШалтгаан байх ЁСТОЙ. (inherited)</sch:assert>
      <sch:assert test="not(f:*[starts-with(local-name(.), 'value')] and (for $coding in f:code/f:coding return f:component/f:code/f:coding[f:code/@value=$coding/f:code/@value] [f:system/@value=$coding/f:system/@value]))">Хэрэв Ажиглалт.код нь Ажиглалт.бүрэлдхүүнХэсэг.кодтой адилхан бол кодтой холбоотой утгын элемент байх ЁСГҮЙ. (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.meta</sch:title>
    <sch:rule context="f:Observation/f:meta">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.implicitRules</sch:title>
    <sch:rule context="f:Observation/f:implicitRules">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.language</sch:title>
    <sch:rule context="f:Observation/f:language">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.text</sch:title>
    <sch:rule context="f:Observation/f:text">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.extension</sch:title>
    <sch:rule context="f:Observation/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.modifierExtension</sch:title>
    <sch:rule context="f:Observation/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.identifier</sch:title>
    <sch:rule context="f:Observation/f:identifier">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.basedOn</sch:title>
    <sch:rule context="f:Observation/f:basedOn">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.partOf</sch:title>
    <sch:rule context="f:Observation/f:partOf">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.status</sch:title>
    <sch:rule context="f:Observation/f:status">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.category</sch:title>
    <sch:rule context="f:Observation/f:category">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:Observation/f:code</sch:title>
    <sch:rule context="f:Observation/f:code">
      <sch:assert test="count(f:id) &lt;= 1">id: maximum cardinality of 'id' is 1</sch:assert>
      <sch:assert test="count(f:text) &lt;= 1">text: maximum cardinality of 'text' is 1</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.code</sch:title>
    <sch:rule context="f:Observation/f:code">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.code.extension</sch:title>
    <sch:rule context="f:Observation/f:code/f:extension">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Must have either extensions or value[x], not both</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:Observation/f:code/f:coding</sch:title>
    <sch:rule context="f:Observation/f:code/f:coding">
      <sch:assert test="count(f:id) &lt;= 1">id: maximum cardinality of 'id' is 1</sch:assert>
      <sch:assert test="count(f:system) &lt;= 1">system: maximum cardinality of 'system' is 1</sch:assert>
      <sch:assert test="count(f:version) &lt;= 1">version: maximum cardinality of 'version' is 1</sch:assert>
      <sch:assert test="count(f:code) &lt;= 1">code: maximum cardinality of 'code' is 1</sch:assert>
      <sch:assert test="count(f:display) &lt;= 1">display: maximum cardinality of 'display' is 1</sch:assert>
      <sch:assert test="count(f:userSelected) &lt;= 1">userSelected: maximum cardinality of 'userSelected' is 1</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.code.coding</sch:title>
    <sch:rule context="f:Observation/f:code/f:coding">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.code.coding.extension</sch:title>
    <sch:rule context="f:Observation/f:code/f:coding/f:extension">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Must have either extensions or value[x], not both</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.code.coding.system</sch:title>
    <sch:rule context="f:Observation/f:code/f:coding/f:system">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.code.coding.version</sch:title>
    <sch:rule context="f:Observation/f:code/f:coding/f:version">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.code.coding.code</sch:title>
    <sch:rule context="f:Observation/f:code/f:coding/f:code">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.code.coding.display</sch:title>
    <sch:rule context="f:Observation/f:code/f:coding/f:display">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.code.coding.userSelected</sch:title>
    <sch:rule context="f:Observation/f:code/f:coding/f:userSelected">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.code.text</sch:title>
    <sch:rule context="f:Observation/f:code/f:text">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.subject</sch:title>
    <sch:rule context="f:Observation/f:subject">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.focus</sch:title>
    <sch:rule context="f:Observation/f:focus">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.encounter</sch:title>
    <sch:rule context="f:Observation/f:encounter">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.effective[x] 1</sch:title>
    <sch:rule context="f:Observation/f:effective[x]">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.issued</sch:title>
    <sch:rule context="f:Observation/f:issued">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.performer</sch:title>
    <sch:rule context="f:Observation/f:performer">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.value[x] 1</sch:title>
    <sch:rule context="f:Observation/f:value[x]">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.value[x].extension 1</sch:title>
    <sch:rule context="f:Observation/f:value[x]/f:extension">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Must have either extensions or value[x], not both</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.value[x].value 1</sch:title>
    <sch:rule context="f:Observation/f:value[x]/f:value">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.value[x].comparator 1</sch:title>
    <sch:rule context="f:Observation/f:value[x]/f:comparator">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.value[x].unit 1</sch:title>
    <sch:rule context="f:Observation/f:value[x]/f:unit">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.value[x].system 1</sch:title>
    <sch:rule context="f:Observation/f:value[x]/f:system">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.value[x].code 1</sch:title>
    <sch:rule context="f:Observation/f:value[x]/f:code">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.dataAbsentReason</sch:title>
    <sch:rule context="f:Observation/f:dataAbsentReason">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.interpretation</sch:title>
    <sch:rule context="f:Observation/f:interpretation">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.note</sch:title>
    <sch:rule context="f:Observation/f:note">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.bodySite</sch:title>
    <sch:rule context="f:Observation/f:bodySite">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.method</sch:title>
    <sch:rule context="f:Observation/f:method">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.specimen</sch:title>
    <sch:rule context="f:Observation/f:specimen">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.device</sch:title>
    <sch:rule context="f:Observation/f:device">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:Observation/f:referenceRange</sch:title>
    <sch:rule context="f:Observation/f:referenceRange">
      <sch:assert test="count(f:type) &lt;= 0">type: maximum cardinality of 'type' is 0</sch:assert>
      <sch:assert test="count(f:text) &lt;= 0">text: maximum cardinality of 'text' is 0</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.referenceRange</sch:title>
    <sch:rule context="f:Observation/f:referenceRange">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="(exists(f:low) or exists(f:high)or exists(f:text))">Дор хаяж бага эсвэл өндөр эсвэл текст байх ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.referenceRange.extension</sch:title>
    <sch:rule context="f:Observation/f:referenceRange/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.referenceRange.modifierExtension</sch:title>
    <sch:rule context="f:Observation/f:referenceRange/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.referenceRange.low</sch:title>
    <sch:rule context="f:Observation/f:referenceRange/f:low">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.referenceRange.high</sch:title>
    <sch:rule context="f:Observation/f:referenceRange/f:high">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.referenceRange.type</sch:title>
    <sch:rule context="f:Observation/f:referenceRange/f:type">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.referenceRange.appliesTo</sch:title>
    <sch:rule context="f:Observation/f:referenceRange/f:appliesTo">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.referenceRange.age</sch:title>
    <sch:rule context="f:Observation/f:referenceRange/f:age">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.referenceRange.text</sch:title>
    <sch:rule context="f:Observation/f:referenceRange/f:text">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.hasMember</sch:title>
    <sch:rule context="f:Observation/f:hasMember">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.derivedFrom</sch:title>
    <sch:rule context="f:Observation/f:derivedFrom">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:Observation/f:component</sch:title>
    <sch:rule context="f:Observation/f:component">
      <sch:assert test="count(f:value[x]) &gt;= 1">value[x]: minimum cardinality of 'value[x]' is 1</sch:assert>
      <sch:assert test="count(f:dataAbsentReason) &lt;= 0">dataAbsentReason: maximum cardinality of 'dataAbsentReason' is 0</sch:assert>
      <sch:assert test="count(f:interpretation) &gt;= 1">interpretation: minimum cardinality of 'interpretation' is 1</sch:assert>
      <sch:assert test="count(f:interpretation) &lt;= 1">interpretation: maximum cardinality of 'interpretation' is 1</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component</sch:title>
    <sch:rule context="f:Observation/f:component">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.extension</sch:title>
    <sch:rule context="f:Observation/f:component/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.modifierExtension</sch:title>
    <sch:rule context="f:Observation/f:component/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.code</sch:title>
    <sch:rule context="f:Observation/f:component/f:code">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.value[x] 1</sch:title>
    <sch:rule context="f:Observation/f:component/f:value[x]">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.dataAbsentReason</sch:title>
    <sch:rule context="f:Observation/f:component/f:dataAbsentReason">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.interpretation</sch:title>
    <sch:rule context="f:Observation/f:component/f:interpretation">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:Observation/f:component/f:referenceRange</sch:title>
    <sch:rule context="f:Observation/f:component/f:referenceRange">
      <sch:assert test="count(f:id) &lt;= 1">id: maximum cardinality of 'id' is 1</sch:assert>
      <sch:assert test="count(f:low) &lt;= 1">low: maximum cardinality of 'low' is 1</sch:assert>
      <sch:assert test="count(f:high) &lt;= 1">high: maximum cardinality of 'high' is 1</sch:assert>
      <sch:assert test="count(f:type) &lt;= 0">type: maximum cardinality of 'type' is 0</sch:assert>
      <sch:assert test="count(f:appliesTo) &lt;= 0">appliesTo: maximum cardinality of 'appliesTo' is 0</sch:assert>
      <sch:assert test="count(f:age) &lt;= 0">age: maximum cardinality of 'age' is 0</sch:assert>
      <sch:assert test="count(f:text) &lt;= 0">text: maximum cardinality of 'text' is 0</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.referenceRange</sch:title>
    <sch:rule context="f:Observation/f:component/f:referenceRange">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.referenceRange.extension</sch:title>
    <sch:rule context="f:Observation/f:component/f:referenceRange/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.referenceRange.modifierExtension</sch:title>
    <sch:rule context="f:Observation/f:component/f:referenceRange/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.referenceRange.low</sch:title>
    <sch:rule context="f:Observation/f:component/f:referenceRange/f:low">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.referenceRange.high</sch:title>
    <sch:rule context="f:Observation/f:component/f:referenceRange/f:high">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.referenceRange.type</sch:title>
    <sch:rule context="f:Observation/f:component/f:referenceRange/f:type">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.referenceRange.appliesTo</sch:title>
    <sch:rule context="f:Observation/f:component/f:referenceRange/f:appliesTo">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.referenceRange.age</sch:title>
    <sch:rule context="f:Observation/f:component/f:referenceRange/f:age">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Observation.component.referenceRange.text</sch:title>
    <sch:rule context="f:Observation/f:component/f:referenceRange/f:text">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
</sch:schema>
