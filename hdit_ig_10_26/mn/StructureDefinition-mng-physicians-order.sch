<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
  <sch:ns prefix="f" uri="http://hl7.org/fhir"/>
  <sch:ns prefix="h" uri="http://www.w3.org/1999/xhtml"/>
  <!-- 
    This file contains just the constraints for the profile ЦахимШилжүүлэгМн
    It includes the base constraints for the resource as well.
    Because of the way that schematrons and containment work, 
    you may need to use this schematron fragment to build a, 
    single schematron that validates contained resources (if you have any) 
  -->
  <sch:pattern>
    <sch:title>f:ServiceRequest</sch:title>
    <sch:rule context="f:ServiceRequest">
      <sch:assert test="count(f:extension[@url = 'hc-id']) &lt;= 1">extension with URL = 'hc-id': maximum cardinality of 'extension' is 1</sch:assert>
      <sch:assert test="count(f:extension[@url = 'http://fhir.mn/StructureDefinition/healthcare-service']) &gt;= 1">extension with URL = 'http://fhir.mn/StructureDefinition/healthcare-service': minimum cardinality of 'extension' is 1</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest</sch:title>
    <sch:rule context="f:ServiceRequest">
      <sch:assert test="not(parent::f:contained and f:contained)">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол, тэрээр дотроо багтсан хэрэглүүр агуулах ЁСГҮЙ. (inherited)</sch:assert>
      <sch:assert test="not(exists(for $id in f:contained/*/f:id/@value return $contained[not(parent::*/descendant::f:reference/@value=concat('#', $contained/*/id/@value) or descendant::f:reference[@value='#'])]))">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол, тэрээр хэрэглүүрийн хаа нэгтэйгээс заагдсан байх ЁСТОЙ эсвэл агуулж буй хэрэглүүр рүү заах ЁСТОЙ. (inherited)</sch:assert>
      <sch:assert test="not(exists(f:contained/*/f:meta/f:versionId)) and not(exists(f:contained/*/f:meta/f:lastUpdated))">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол тэрээр meta.versionId эсвэл meta.lastUpdated элементтэй байх ЁСГҮЙ. (inherited)</sch:assert>
      <sch:assert test="not(exists(f:contained/*/f:meta/f:security))">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол, аюулгүй байдлын шошготой байх ЁСГҮЙ. (inherited)</sch:assert>
      <sch:assert test="exists(f:text/h:div)">Зохион байгуулалтыг сайн байлгах үүднээс хэрэглүүрүүдэд хүүрнэл хэсэг байх хэрэгтэй. (inherited)</sch:assert>
      <sch:assert test="exists(f:code) or not(exists(f:orderDetail))">зөвхөн код байгаа үед л захиалгынХэсгүүд байна (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.meta</sch:title>
    <sch:rule context="f:ServiceRequest/f:meta">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.implicitRules</sch:title>
    <sch:rule context="f:ServiceRequest/f:implicitRules">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.language</sch:title>
    <sch:rule context="f:ServiceRequest/f:language">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.text</sch:title>
    <sch:rule context="f:ServiceRequest/f:text">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.extension</sch:title>
    <sch:rule context="f:ServiceRequest/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.extension.extension</sch:title>
    <sch:rule context="f:ServiceRequest/f:extension/f:extension">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Must have either extensions or value[x], not both</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.extension.value[x] 1</sch:title>
    <sch:rule context="f:ServiceRequest/f:extension/f:value[x]">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.modifierExtension</sch:title>
    <sch:rule context="f:ServiceRequest/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.extension</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.use</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:use">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.type</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:type">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.type.extension</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:type/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.type.coding</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:type/f:coding">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.type.coding.extension</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:type/f:coding/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.type.coding.system</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:type/f:coding/f:system">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.type.coding.version</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:type/f:coding/f:version">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.type.coding.code</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:type/f:coding/f:code">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.type.coding.display</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:type/f:coding/f:display">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.type.coding.userSelected</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:type/f:coding/f:userSelected">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.type.text</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:type/f:text">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.system</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:system">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.value</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:value">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.period</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:period">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.identifier.assigner</sch:title>
    <sch:rule context="f:ServiceRequest/f:identifier/f:assigner">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.instantiatesCanonical</sch:title>
    <sch:rule context="f:ServiceRequest/f:instantiatesCanonical">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.instantiatesUri</sch:title>
    <sch:rule context="f:ServiceRequest/f:instantiatesUri">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.basedOn</sch:title>
    <sch:rule context="f:ServiceRequest/f:basedOn">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.replaces</sch:title>
    <sch:rule context="f:ServiceRequest/f:replaces">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.requisition</sch:title>
    <sch:rule context="f:ServiceRequest/f:requisition">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.status</sch:title>
    <sch:rule context="f:ServiceRequest/f:status">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.intent</sch:title>
    <sch:rule context="f:ServiceRequest/f:intent">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:ServiceRequest/f:category</sch:title>
    <sch:rule context="f:ServiceRequest/f:category">
      <sch:assert test="count(f:extension[@url = 'http://fhir.mn/StructureDefinition/category-details']) &lt;= 1">extension with URL = 'http://fhir.mn/StructureDefinition/category-details': maximum cardinality of 'extension' is 1</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.category</sch:title>
    <sch:rule context="f:ServiceRequest/f:category">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.category.extension</sch:title>
    <sch:rule context="f:ServiceRequest/f:category/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:ServiceRequest/f:category/f:extension</sch:title>
    <sch:rule context="f:ServiceRequest/f:category/f:extension">
      <sch:assert test="count(f:extension[@url = 'reason-detail']) &gt;= 1">extension with URL = 'reason-detail': minimum cardinality of 'extension' is 1</sch:assert>
      <sch:assert test="count(f:extension[@url = 'reason-detail']) &lt;= 1">extension with URL = 'reason-detail': maximum cardinality of 'extension' is 1</sch:assert>
      <sch:assert test="count(f:extension[@url = 'comment']) &lt;= 1">extension with URL = 'comment': maximum cardinality of 'extension' is 1</sch:assert>
      <sch:assert test="count(f:extension[@url = 'http://fhir.mn/StructureDefinition/patientAge']) &gt;= 1">extension with URL = 'http://fhir.mn/StructureDefinition/patientAge': minimum cardinality of 'extension' is 1</sch:assert>
      <sch:assert test="count(f:extension[@url = 'http://fhir.mn/StructureDefinition/patientAge']) &lt;= 1">extension with URL = 'http://fhir.mn/StructureDefinition/patientAge': maximum cardinality of 'extension' is 1</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.category.extension.extension</sch:title>
    <sch:rule context="f:ServiceRequest/f:category/f:extension/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.category.extension.extension.extension</sch:title>
    <sch:rule context="f:ServiceRequest/f:category/f:extension/f:extension/f:extension">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Must have either extensions or value[x], not both</sch:assert>
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Must have either extensions or value[x], not both</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.category.extension.extension.value[x] 1</sch:title>
    <sch:rule context="f:ServiceRequest/f:category/f:extension/f:extension/f:value[x]">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.category.extension.value[x] 1</sch:title>
    <sch:rule context="f:ServiceRequest/f:category/f:extension/f:value[x]">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.category.coding</sch:title>
    <sch:rule context="f:ServiceRequest/f:category/f:coding">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.category.text</sch:title>
    <sch:rule context="f:ServiceRequest/f:category/f:text">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.priority</sch:title>
    <sch:rule context="f:ServiceRequest/f:priority">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.doNotPerform</sch:title>
    <sch:rule context="f:ServiceRequest/f:doNotPerform">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.code</sch:title>
    <sch:rule context="f:ServiceRequest/f:code">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.orderDetail</sch:title>
    <sch:rule context="f:ServiceRequest/f:orderDetail">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.quantity[x] 1</sch:title>
    <sch:rule context="f:ServiceRequest/f:quantity[x]">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.subject</sch:title>
    <sch:rule context="f:ServiceRequest/f:subject">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.encounter</sch:title>
    <sch:rule context="f:ServiceRequest/f:encounter">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.occurrence[x] 1</sch:title>
    <sch:rule context="f:ServiceRequest/f:occurrence[x]">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.asNeeded[x] 1</sch:title>
    <sch:rule context="f:ServiceRequest/f:asNeeded[x]">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.authoredOn</sch:title>
    <sch:rule context="f:ServiceRequest/f:authoredOn">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.requester</sch:title>
    <sch:rule context="f:ServiceRequest/f:requester">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.performerType</sch:title>
    <sch:rule context="f:ServiceRequest/f:performerType">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.performer</sch:title>
    <sch:rule context="f:ServiceRequest/f:performer">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.locationCode</sch:title>
    <sch:rule context="f:ServiceRequest/f:locationCode">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.locationReference</sch:title>
    <sch:rule context="f:ServiceRequest/f:locationReference">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.reasonCode</sch:title>
    <sch:rule context="f:ServiceRequest/f:reasonCode">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.reasonReference</sch:title>
    <sch:rule context="f:ServiceRequest/f:reasonReference">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.insurance</sch:title>
    <sch:rule context="f:ServiceRequest/f:insurance">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.supportingInfo</sch:title>
    <sch:rule context="f:ServiceRequest/f:supportingInfo">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.specimen</sch:title>
    <sch:rule context="f:ServiceRequest/f:specimen">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.bodySite</sch:title>
    <sch:rule context="f:ServiceRequest/f:bodySite">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.note</sch:title>
    <sch:rule context="f:ServiceRequest/f:note">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.patientInstruction</sch:title>
    <sch:rule context="f:ServiceRequest/f:patientInstruction">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>ServiceRequest.relevantHistory</sch:title>
    <sch:rule context="f:ServiceRequest/f:relevantHistory">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
</sch:schema>
