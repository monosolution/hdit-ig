<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
  <sch:ns prefix="f" uri="http://hl7.org/fhir"/>
  <sch:ns prefix="h" uri="http://www.w3.org/1999/xhtml"/>
  <!-- 
    This file contains just the constraints for the profile ЭнкантурМн
    It includes the base constraints for the resource as well.
    Because of the way that schematrons and containment work, 
    you may need to use this schematron fragment to build a, 
    single schematron that validates contained resources (if you have any) 
  -->
  <sch:pattern>
    <sch:title>f:Encounter</sch:title>
    <sch:rule context="f:Encounter">
      <sch:assert test="count(f:extension[@url = 'http://hl7.org/fhir/StructureDefinition/encounter-modeOfArrival']) &gt;= 1">extension with URL = 'http://hl7.org/fhir/StructureDefinition/encounter-modeOfArrival': minimum cardinality of 'extension' is 1</sch:assert>
      <sch:assert test="count(f:extension[@url = 'http://hl7.org/fhir/StructureDefinition/encounter-modeOfArrival']) &lt;= 1">extension with URL = 'http://hl7.org/fhir/StructureDefinition/encounter-modeOfArrival': maximum cardinality of 'extension' is 1</sch:assert>
      <sch:assert test="count(f:statusHistory) &lt;= 0">statusHistory: maximum cardinality of 'statusHistory' is 0</sch:assert>
      <sch:assert test="count(f:classHistory) &lt;= 0">classHistory: maximum cardinality of 'classHistory' is 0</sch:assert>
      <sch:assert test="count(f:type) &lt;= 0">type: maximum cardinality of 'type' is 0</sch:assert>
      <sch:assert test="count(f:serviceType) &lt;= 0">serviceType: maximum cardinality of 'serviceType' is 0</sch:assert>
      <sch:assert test="count(f:priority) &lt;= 0">priority: maximum cardinality of 'priority' is 0</sch:assert>
      <sch:assert test="count(f:subject) &gt;= 1">subject: minimum cardinality of 'subject' is 1</sch:assert>
      <sch:assert test="count(f:episodeOfCare) &lt;= 0">episodeOfCare: maximum cardinality of 'episodeOfCare' is 0</sch:assert>
      <sch:assert test="count(f:basedOn) &lt;= 0">basedOn: maximum cardinality of 'basedOn' is 0</sch:assert>
      <sch:assert test="count(f:appointment) &lt;= 0">appointment: maximum cardinality of 'appointment' is 0</sch:assert>
      <sch:assert test="count(f:period) &gt;= 1">period: minimum cardinality of 'period' is 1</sch:assert>
      <sch:assert test="count(f:length) &gt;= 1">length: minimum cardinality of 'length' is 1</sch:assert>
      <sch:assert test="count(f:account) &lt;= 0">account: maximum cardinality of 'account' is 0</sch:assert>
      <sch:assert test="count(f:hospitalization) &gt;= 1">hospitalization: minimum cardinality of 'hospitalization' is 1</sch:assert>
      <sch:assert test="count(f:location) &lt;= 0">location: maximum cardinality of 'location' is 0</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter</sch:title>
    <sch:rule context="f:Encounter">
      <sch:assert test="not(parent::f:contained and f:contained)">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол, тэрээр дотроо багтсан хэрэглүүр агуулах ЁСГҮЙ. (inherited)</sch:assert>
      <sch:assert test="not(exists(for $id in f:contained/*/f:id/@value return $contained[not(parent::*/descendant::f:reference/@value=concat('#', $contained/*/id/@value) or descendant::f:reference[@value='#'])]))">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол, тэрээр хэрэглүүрийн хаа нэгтэйгээс заагдсан байх ЁСТОЙ эсвэл агуулж буй хэрэглүүр рүү заах ЁСТОЙ. (inherited)</sch:assert>
      <sch:assert test="not(exists(f:contained/*/f:meta/f:versionId)) and not(exists(f:contained/*/f:meta/f:lastUpdated))">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол тэрээр meta.versionId эсвэл meta.lastUpdated элементтэй байх ЁСГҮЙ. (inherited)</sch:assert>
      <sch:assert test="not(exists(f:contained/*/f:meta/f:security))">Хэрвээ хэрэглүүр өөр хэрэглүүрийн дотор агуулагдаж байгаа бол, аюулгүй байдлын шошготой байх ЁСГҮЙ. (inherited)</sch:assert>
      <sch:assert test="exists(f:text/h:div)">Зохион байгуулалтыг сайн байлгах үүднээс хэрэглүүрүүдэд хүүрнэл хэсэг байх хэрэгтэй. (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.meta</sch:title>
    <sch:rule context="f:Encounter/f:meta">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.implicitRules</sch:title>
    <sch:rule context="f:Encounter/f:implicitRules">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.language</sch:title>
    <sch:rule context="f:Encounter/f:language">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.text</sch:title>
    <sch:rule context="f:Encounter/f:text">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.extension</sch:title>
    <sch:rule context="f:Encounter/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), 'value')])">Must have either extensions or value[x], not both (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:Encounter/f:extension</sch:title>
    <sch:rule context="f:Encounter/f:extension">
      <sch:assert test="count(f:id) &lt;= 1">id: maximum cardinality of 'id' is 1</sch:assert>
      <sch:assert test="count(f:url) &gt;= 1">url: minimum cardinality of 'url' is 1</sch:assert>
      <sch:assert test="count(f:url) &lt;= 1">url: maximum cardinality of 'url' is 1</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.extension.extension</sch:title>
    <sch:rule context="f:Encounter/f:extension/f:extension">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Must have either extensions or value[x], not both</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.extension.value[x] 1</sch:title>
    <sch:rule context="f:Encounter/f:extension/f:value[x]">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.modifierExtension</sch:title>
    <sch:rule context="f:Encounter/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.identifier</sch:title>
    <sch:rule context="f:Encounter/f:identifier">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.status</sch:title>
    <sch:rule context="f:Encounter/f:status">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.statusHistory</sch:title>
    <sch:rule context="f:Encounter/f:statusHistory">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.statusHistory.extension</sch:title>
    <sch:rule context="f:Encounter/f:statusHistory/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.statusHistory.modifierExtension</sch:title>
    <sch:rule context="f:Encounter/f:statusHistory/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.statusHistory.status</sch:title>
    <sch:rule context="f:Encounter/f:statusHistory/f:status">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.statusHistory.period</sch:title>
    <sch:rule context="f:Encounter/f:statusHistory/f:period">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.class</sch:title>
    <sch:rule context="f:Encounter/f:class">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.classHistory</sch:title>
    <sch:rule context="f:Encounter/f:classHistory">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.classHistory.extension</sch:title>
    <sch:rule context="f:Encounter/f:classHistory/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.classHistory.modifierExtension</sch:title>
    <sch:rule context="f:Encounter/f:classHistory/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.classHistory.class</sch:title>
    <sch:rule context="f:Encounter/f:classHistory/f:class">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.classHistory.period</sch:title>
    <sch:rule context="f:Encounter/f:classHistory/f:period">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.type</sch:title>
    <sch:rule context="f:Encounter/f:type">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.serviceType</sch:title>
    <sch:rule context="f:Encounter/f:serviceType">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.priority</sch:title>
    <sch:rule context="f:Encounter/f:priority">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.subject</sch:title>
    <sch:rule context="f:Encounter/f:subject">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.episodeOfCare</sch:title>
    <sch:rule context="f:Encounter/f:episodeOfCare">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.basedOn</sch:title>
    <sch:rule context="f:Encounter/f:basedOn">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.participant</sch:title>
    <sch:rule context="f:Encounter/f:participant">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.participant.extension</sch:title>
    <sch:rule context="f:Encounter/f:participant/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.participant.modifierExtension</sch:title>
    <sch:rule context="f:Encounter/f:participant/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.participant.type</sch:title>
    <sch:rule context="f:Encounter/f:participant/f:type">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.participant.period</sch:title>
    <sch:rule context="f:Encounter/f:participant/f:period">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.participant.individual</sch:title>
    <sch:rule context="f:Encounter/f:participant/f:individual">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:Encounter/f:participant</sch:title>
    <sch:rule context="f:Encounter/f:participant">
      <sch:assert test="count(f:type) &gt;= 1">type: minimum cardinality of 'type' is 1</sch:assert>
      <sch:assert test="count(f:type) &gt;= 1">type: minimum cardinality of 'type' is 1</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.appointment</sch:title>
    <sch:rule context="f:Encounter/f:appointment">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:Encounter/f:period</sch:title>
    <sch:rule context="f:Encounter/f:period">
      <sch:assert test="count(f:id) &lt;= 1">id: maximum cardinality of 'id' is 1</sch:assert>
      <sch:assert test="count(f:start) &gt;= 1">start: minimum cardinality of 'start' is 1</sch:assert>
      <sch:assert test="count(f:start) &lt;= 1">start: maximum cardinality of 'start' is 1</sch:assert>
      <sch:assert test="count(f:end) &gt;= 1">end: minimum cardinality of 'end' is 1</sch:assert>
      <sch:assert test="count(f:end) &lt;= 1">end: maximum cardinality of 'end' is 1</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.period</sch:title>
    <sch:rule context="f:Encounter/f:period">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.period.extension</sch:title>
    <sch:rule context="f:Encounter/f:period/f:extension">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Must have either extensions or value[x], not both</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.period.start</sch:title>
    <sch:rule context="f:Encounter/f:period/f:start">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.period.end</sch:title>
    <sch:rule context="f:Encounter/f:period/f:end">
      <sch:assert test="@value|f:*|h:div">All FHIR elements must have a @value or children (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.length</sch:title>
    <sch:rule context="f:Encounter/f:length">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.reasonCode</sch:title>
    <sch:rule context="f:Encounter/f:reasonCode">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.reasonReference</sch:title>
    <sch:rule context="f:Encounter/f:reasonReference">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.diagnosis</sch:title>
    <sch:rule context="f:Encounter/f:diagnosis">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.diagnosis.extension</sch:title>
    <sch:rule context="f:Encounter/f:diagnosis/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.diagnosis.modifierExtension</sch:title>
    <sch:rule context="f:Encounter/f:diagnosis/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.diagnosis.condition</sch:title>
    <sch:rule context="f:Encounter/f:diagnosis/f:condition">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.diagnosis.use</sch:title>
    <sch:rule context="f:Encounter/f:diagnosis/f:use">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.diagnosis.rank</sch:title>
    <sch:rule context="f:Encounter/f:diagnosis/f:rank">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:Encounter/f:diagnosis</sch:title>
    <sch:rule context="f:Encounter/f:diagnosis">
      <sch:assert test="count(f:use) &gt;= 1">use: minimum cardinality of 'use' is 1</sch:assert>
      <sch:assert test="count(f:use) &gt;= 1">use: minimum cardinality of 'use' is 1</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.account</sch:title>
    <sch:rule context="f:Encounter/f:account">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>f:Encounter/f:hospitalization</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization">
      <sch:assert test="count(f:extension[@url = 'http://fhir.mn/StructureDefinition/handover-requested']) &lt;= 1">extension with URL = 'http://fhir.mn/StructureDefinition/handover-requested': maximum cardinality of 'extension' is 1</sch:assert>
      <sch:assert test="count(f:extension[@url = 'http://fhir.mn/StructureDefinition/handover-performed']) &lt;= 1">extension with URL = 'http://fhir.mn/StructureDefinition/handover-performed': maximum cardinality of 'extension' is 1</sch:assert>
      <sch:assert test="count(f:extension[@url = 'http://fhir.mn/StructureDefinition/handover-person']) &lt;= 1">extension with URL = 'http://fhir.mn/StructureDefinition/handover-person': maximum cardinality of 'extension' is 1</sch:assert>
      <sch:assert test="count(f:extension[@url = 'http://fhir.mn/StructureDefinition/receiving-person']) &lt;= 1">extension with URL = 'http://fhir.mn/StructureDefinition/receiving-person': maximum cardinality of 'extension' is 1</sch:assert>
      <sch:assert test="count(f:preAdmissionIdentifier) &lt;= 0">preAdmissionIdentifier: maximum cardinality of 'preAdmissionIdentifier' is 0</sch:assert>
      <sch:assert test="count(f:origin) &lt;= 0">origin: maximum cardinality of 'origin' is 0</sch:assert>
      <sch:assert test="count(f:admitSource) &lt;= 0">admitSource: maximum cardinality of 'admitSource' is 0</sch:assert>
      <sch:assert test="count(f:reAdmission) &lt;= 0">reAdmission: maximum cardinality of 'reAdmission' is 0</sch:assert>
      <sch:assert test="count(f:dietPreference) &lt;= 0">dietPreference: maximum cardinality of 'dietPreference' is 0</sch:assert>
      <sch:assert test="count(f:specialCourtesy) &lt;= 0">specialCourtesy: maximum cardinality of 'specialCourtesy' is 0</sch:assert>
      <sch:assert test="count(f:specialArrangement) &lt;= 0">specialArrangement: maximum cardinality of 'specialArrangement' is 0</sch:assert>
      <sch:assert test="count(f:dischargeDisposition) &gt;= 1">dischargeDisposition: minimum cardinality of 'dischargeDisposition' is 1</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization.extension</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), 'value')])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), 'value')])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), 'value')])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), 'value')])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization.modifierExtension</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization.preAdmissionIdentifier</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization/f:preAdmissionIdentifier">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization.origin</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization/f:origin">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization.admitSource</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization/f:admitSource">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization.reAdmission</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization/f:reAdmission">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization.dietPreference</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization/f:dietPreference">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization.specialCourtesy</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization/f:specialCourtesy">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization.specialArrangement</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization/f:specialArrangement">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization.destination</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization/f:destination">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.hospitalization.dischargeDisposition</sch:title>
    <sch:rule context="f:Encounter/f:hospitalization/f:dischargeDisposition">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.location</sch:title>
    <sch:rule context="f:Encounter/f:location">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.location.extension</sch:title>
    <sch:rule context="f:Encounter/f:location/f:extension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.location.modifierExtension</sch:title>
    <sch:rule context="f:Encounter/f:location/f:modifierExtension">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
      <sch:assert test="exists(f:extension)!=exists(f:*[starts-with(local-name(.), &quot;value&quot;)])">Өргөтгөл эсвэл утга[x]-ийн аль нэгийг агуулах ёстой, хоёуланг нь агуулж болохгүй (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.location.location</sch:title>
    <sch:rule context="f:Encounter/f:location/f:location">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.location.status</sch:title>
    <sch:rule context="f:Encounter/f:location/f:status">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.location.physicalType</sch:title>
    <sch:rule context="f:Encounter/f:location/f:physicalType">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.location.period</sch:title>
    <sch:rule context="f:Encounter/f:location/f:period">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.serviceProvider</sch:title>
    <sch:rule context="f:Encounter/f:serviceProvider">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
  <sch:pattern>
    <sch:title>Encounter.partOf</sch:title>
    <sch:rule context="f:Encounter/f:partOf">
      <sch:assert test="@value|f:*|h:div">FHIR-ийн бүх элементүүд @утга эсвэл дэд элементийг агуулах ёстой (inherited)</sch:assert>
    </sch:rule>
  </sch:pattern>
</sch:schema>
